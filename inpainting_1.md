# **Poznej FI 2022** - Image InPainting 1


Vyfotil jsi skvělou fotku, ale do záběru se ti postavil horlivý turista? Chceš opravit obrázek, který je překrytý textem? Zajímá tě, jak automaticky detekovat objekty v obraze a jak je z tohoto obrazu vymazat? V tomto skriptu si takové věci vyzkoušíme.
Odborně se technice dokreslování říká **Image Inpainting**. Víc se o ní můžeš dočíst například na stránkách Wikipedie: https://en.wikipedia.org/wiki/Inpainting.

*"Inpainting je proces rekonstruování ztracených nebo poškozených částí obrazu nebo videa. V případě uměleckých děl je tento úkol realizován zkušenými malíři a reustauratéry. V digitálním světě, Inpainting odkazuje na aplikaci nebo specializovaný algoritmus, který automaticky doplní chybějící nebo poškozenou část obrazu."*


### **Obsah tutorialu**<br>

> #### **1.        Základy práce s obrazem**
> #### 2. Hledání objektů v obraze
> #### 3. Pokročilé dokreslovací metody

<br>     



## 1. základy práce s obrazem

Kód budeme psát v jazyce Pythonu 3. Editor, který teď používáme, se jmenuje Jupyter Notebook. Jeho výhodou je, že je volně dostupný. Soubor s koncovkou .ipynb lze ale spustit i v jiných editorech. Celý skript je dostupný také na adrese https://gitlab.fi.muni.cz/xlux/poznejfi/

Dříve než začneme, naučíme se, jak používat Jupyter Notebook. V celém skriptu se nachází dva typy buněk: s formátovaným textem v jazyce Markdown (například tato buňka) a kódem v jazyce Python. Ty s kódem poznáš tak, že vedle nich je symbol **[ ]**. Každou buňku lze spustit pomocí klávesové zkratky <b>Ctrl + Enter</b>. Zkratka <b>Shift + Enter</b> tě zároveň posune na další řádek. Při spuštění buňky se vykoná daný kód a za buňku se vypíše výsledek poslední operace. 

###  <span style="color:green">ÚKOL 1: PYTHON V JUPYTERU</span>

**1.1.** Vyzkoušej spuštění následující buňky.


```python
a = 'Výborně! ' 
b = 'Podařilo se ti '
c = 'spustit první buň'
d = 'ku!'
a + b + c + d
```

**1.2.** Pokud nechceš nějaký řádek kódu vykonat, napiš před něj znak #

        # print('..tento řádek se nemá vypsat..')






```python
""" ÚKOL 1.2 """
'zakomentuj symbolem # řádky 2 a 4, které se nemají provést'

print('OBČANÉ ATLANTIDY!')
print('... Tento řádek se nemá vypsat...')
print('Zachovejte klid a rozvahu!')
print('... Ani tento: lspo p dsppdskpodsk fdspo.')
print('Potopení jižní části ostrova je jen dočasné!')
```


```python
""" ÚKOL 1.2 - SOLUTION """

print('OBČANÉ ATLANTIDY!')
#print('... Tento řádek se nemá vypsat...')
print('Zachovejte klid a rozvahu!')
#print('... Ani tento: lspo p dsppdskpodsk fdspo.')
print('Potopení jižní části ostrova je jen dočasné!')
```

**1.3.** Teď oprav následující příklad a nech Python správně spočítat, kolik je 120 * 34.




```python
""" ÚKOL 1.3 """

print(f'120 * 34 = {2*2}')
```


```python
""" ÚKOL 1.3 - SOLUTION """

print(f'120 * 34 = {120*34}')
```

## Obrazová data

Naimportujeme si knihovny, které se nám budou hodit ke zpracování obrázků. Pro import stačí spustit následující buňku.  
Pokud by tě knihovny zajímaly víc, najdi si jejich dokumentaci. (např. https://docs.python.org/3/library/os.html)

* **os** -- Přístup k souborům (operační systém).
* **numpy** -- Budeme používat ke snazší práci s hodnotami pixelů.
* **skimage** -- Vědecká knihovna pro zpracování obrazových dat
* **matplotlib** -- Zobrazování obrazových dat.




```python
# import knihoven
import os
import numpy as np
import matplotlib

from skimage import io

%matplotlib tk
import poznejFI_tools as fi
```

*INFO: Do skriptu můžeš přidávat nové buňky a v nich si zkoušet vlastní příkazy.
Novou buňku přidáš pomocí tlačítka ***+*** v hlavičce notebooku.*

###  <span style="color:green">ÚKOL 2: POLE HODNOT</span>
V nových buňkách vyřeš následující příklady:

**2.1.** Jaký je součet čísel 1 - 5? Použij funkci `np.sum()`. Té jako parametr zadej seznam čísel která se mají sečíst.

        [1, 2, 3, 4, 5]






```python
''' 2.1 - SOLUTION '''
res = np.sum([1, 2, 3, 4, 5])
print(res)
```

**2.2** Jaká je průměrná hodnota v poli `a`? Vyřeš v nové buňce.

        a = np.array([[10, 24, 42, 12, 10],
                      [30, 26, 40,  7,  4],
                      [50, 42, 15,  6, 27]])
        np.mean(a)






```python
''' 2.2 - SOLUTION '''
a = np.array([[10, 24, 42, 12, 10],
               [30, 26, 40,  7,  4],
               [50, 42, 15,  6, 27]])
res = np.mean(a)
print(res)
```

Digitální obraz není nic jiného než **dvourozměrné pole hodnot**. Každá hodnota odpovídá hodnotě pixelu na dané pozici. V následujícícm cvičení budeme pracovat s polem hodnot jako s obrazovými daty.

###  <span style="color:green">ÚKOL 3: OBRAZOVÁ DATA</span>

**3.1.** Spuštěním následující buňky vytvoř nulový obrázek s rozměry 9 x 9 pixelů a zobraz ho.




```python
# Definice pole hodnot
img = np.zeros((9, 9))

# zobrazení obrázku jako mřížky pixelů
fi.show_pixels(img)
```

**3.2.** Do kódu v předchozí buňce přidej příkaz, který obarví prostřední pixel na bílo. Výsledek zobraz.

       img[ INDEX1, INDEX2 ] = 255






```python
''' ÚKOL 3.2. - SOLUTION'''
# Definice obrazu
img = np.zeros((9, 9))
img[4, 4] = 255

# zobrazení obrázku jako mřížky pixelů
fi.show_pixels(img)
```

**3.3.** Definuj obrázek následující maticí a zobraz si ho:

       img = np.array([[67, 51, 36, 66, 64, 53, 47, 50, 27, 65, 58],
                       [62, 51, 47, 23, 55, 63, 52, 27, 50, 50, 50],
                       [52, 65, 41, 26, 32, 23, 23, 33, 41, 47, 65],
                       [67, 33, 22, 63, 41, 37, 26, 56, 34, 27, 49],
                       [28, 27, 29, 37, 41, 32, 41, 37, 31, 22, 39],
                       [31, 48, 27, 33, 37, 23, 27, 37, 40, 59, 29],
                       [23, 51, 30, 54, 53, 61, 57, 58, 42, 52, 29],
                       [62, 54, 58, 37, 38, 54, 24, 31, 60, 65, 54]])
                         






```python
''' ÚKOL 3.3. - SOLUTION'''
# Definice obrazu
img = np.array([[67, 51, 36, 66, 64, 53, 47, 50, 27, 65, 58],
                [62, 51, 47, 23, 55, 63, 52, 27, 50, 50, 50],
                [52, 65, 41, 26, 32, 23, 23, 33, 41, 47, 65],
                [67, 33, 22, 63, 41, 37, 26, 56, 34, 27, 49],
                [28, 27, 29, 37, 41, 32, 41, 37, 31, 22, 39],
                [31, 48, 27, 33, 37, 23, 27, 37, 40, 59, 29],
                [23, 51, 30, 54, 53, 61, 57, 58, 42, 52, 29],
                [62, 54, 58, 37, 38, 54, 24, 31, 60, 65, 54]])
# zobrazení obrázku jako mřížky pixelů
fi.show_pixels(img)
```

**3.4.** V poli `img` najdi všechny pixely s hodnotou větší než 42 a výsledný obrázek zobraz.'




```python
''' ÚKOL 3.2. - SOLUTION'''
# Definice obrazu
img = np.array([[67, 51, 36, 66, 64, 53, 47, 50, 27, 65, 58],
                [62, 51, 47, 23, 55, 63, 52, 27, 50, 50, 50],
                [52, 65, 41, 26, 32, 23, 23, 33, 41, 47, 65],
                [67, 33, 22, 63, 41, 37, 26, 56, 34, 27, 49],
                [28, 27, 29, 37, 41, 32, 41, 37, 31, 22, 39],
                [31, 48, 27, 33, 37, 23, 27, 37, 40, 59, 29],
                [23, 51, 30, 54, 53, 61, 57, 58, 42, 52, 29],
                [62, 54, 58, 37, 38, 54, 24, 31, 60, 65, 54]]) > 42
# zobrazení obrázku jako mřížky pixelů
fi.show_pixels(img)
```


```python
# zobrazení bez pixelové mřížky
fi.imshow(img)
```

Obrazy samozřejmě nebudeme jen vytvářet, ale i načítat z obrazových souborů, například s koncovkou '.jpg', '.tif' nebo '.png'.

### <span style="color:green">ÚKOL 4: NAČTENÍ OBRAZOVÝCH DAT</span>
**4.1.** Vypiš seznam souborů, které jsou ve složce *images*.




```python
""" ÚKOL 4.1 """

os.listdir('JMÉNO_SLOŽKY')
```


```python
""" ÚKOL 4.1 - SOLUTION """

os.listdir('images')
```

2. Podle vypsaného seznamu si vykresli a prohlédni obrázky ve složce.




```python
""" TODO 2 """

img_name = 'images/NÁZEV_OBRÁZKU'
```


```python
""" TODO 2 - SOLUTION """

img_name = 'images/eiffel_tower.jpg'
```


```python
# načtení obrázku
img = io.imread(img_name)

# zobrazení obrázku
fi.imshow(img)
```

### <span style="color:green">ÚKOL 5: HLEDÁNÍ BAREV V OBRÁZKU</span>

Většina obrázků je v počítači uložena jako rastr pixelů, které nemají jen různé odstíny šedé, ale jsou barevné. Barvy mohou být zakódovány pomocí trojice čísel `[R, G, B]`, která udávají, jaké je v dané barvě množství červené `R`, zelené `G` a modré `B`. Barva **[255, 0, 0]** tedy odpovídá červené, **[255, 255, 255]** je bílá, **[0, 0, 0]** je černá a **[0,255, 255]** je azurová. 




```python
img_name = 'images/lentilky.png'

# načtení obrázku
img = io.imread(img_name)

# zobrazení obrázku
fi.imshow(img)

```

1. Ze složky s obrázky si otevři soubor *lentilky.png* a zjisti průměrné barvy lentilek. V následující buňce je zapiš do proměnných `blue`, `orange`, `red`, `green` a `yellow`.




```python
""" ÚKOL 5.1 """
'nastav hodnoty barev tak, aby odpovídaly barvám lentilek'

blue = [0, 0, 0]
orange = [0, 0, 0]
red = [0, 0, 0]
green = [0, 0, 0]
yellow = [0, 0, 0]
```


```python
""" ÚKOL 5.1 - SOLUTION """

blue = [0, 180, 230]
orange = [250, 150, 0]
red = [250, 60, 50]
green = [140, 230, 30]
yellow = [250, 230, 0]
```


```python
colors = [blue, orange, red, green, yellow]

palette = np.concatenate([np.ones((10, 10, 3), np.uint8) * color for color in colors], axis=1)
fi.imshow(palette)
```

#### Dobrá práce! Pokračuj notebookem číslo 2.

**<span style="color:blue">UŽ UMÍME:</span>**
 > **<span style="color:blue">1. používat buňky v Jupyteru</span>**  
 > **<span style="color:blue">2. vytvořit obrázek</span>**  
 > **<span style="color:blue">3. měnit hodnoty pixelů v obrázku</span>**  
 > **<span style="color:blue">4. načíst a zobrazit obrázek ze souboru</span>**  
 > **<span style="color:blue">5. vyčíst z obrázku hodnoty barev</span>**  

Tento skript vznikl pro prezentaci oboru Vizuální informatika na Fakultě informatiky MU.  
Dotazy nebo připomínky pište na email autora xlux@fi.muni.cz, nebo na email celé skupiny cbia@fi.muni.cz.

https://cbia.fi.muni.cz/  
https://www.fi.muni.cz/




```python

```
