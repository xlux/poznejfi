# **Poznej FI 2022** - Image InPainting 2


Vyfotil jsi skvělou fotku, ale do záběru se ti postavil horlivý turista? Chceš opravit obrázek, který je překrytý textem? Zajímá tě, jak automaticky detekovat objekty v obraze a jak je z tohoto obrazu vymazat? V tomto skriptu si takové věci vyzkoušíme.
Odborně se technice dokreslování říká **Image Inpainting**. Víc se o ní můžeš dočíst například na stránkách Wikipedie: https://en.wikipedia.org/wiki/Inpainting.

*"Inpainting je proces rekonstruování ztracených nebo poškozených částí obrazu nebo videa. V případě uměleckých děl je tento úkol realizován zkušenými malíři a reustauratéry. V digitálním světě, Inpainting odkazuje na aplikaci nebo specializovaný algoritmus, který automaticky doplní chybějící nebo poškozenou část obrazu."*


### **Obsah tutorialu**<br>

> #### 1. Základy práce s obrazem
> #### 2. Hledání objektů v obraze
> #### **3. Pokročilé dokreslovací metody**

<br>     

## 3. Pokročilé dokreslovací metody

Neurální sítě a strojové učení jsou nyní ve vědě velmi populární.
Obě technologie jsou založeny na statistice a umožňují z velkého množství dat nebo obrázků abstrahovat společné vlastnosti.
Podle nich pak můžeme předpovídat vlastnosti obrázků, které jsme ještě neviděli.

**Jak například funguje předvídání toho, jaká další písnička se mi bude líbit?** <i> Představ si, že na Spotify posloucháš pět svých oblíbených kapel. Protože tato služba má velké množství dat, pomocí strojového učení tě zařadí do skupiny s dalšími lidmi, kteří tvé oblíbené kapely také poslouchají. A doporučí ti nějakou jejich oblíbenou kapelu, kterou ty ještě neznáš. </i>

Základem těchto metod je mít opravdu velké množství dat, a pak nad ním udělat statistiku.
Pro vytvoření neurální sítě jsou proto zásadní právě data, na kterých se síť učí. Abychom měli představu, jak se neurální síť bude chovat, je dobré tato data znát.




```python
# import knihoven
import os
import numpy as np
from skimage import io, transform

%matplotlib 
import poznejFI_tools as fi
```


```python
img = io.imread('images/sagrada_familia.jpg')

mask = fi.draw_mask(img)
fi.imshow(img, mask)
```


```python
res = fi.inpaint_AI(img, mask)

fi.imshow(res)
```


```python

```

Síť, kterou budeme používat pro dokreslování obrazových dat, je nazvaná EdgeConnect a byla popsaná v článku https://arxiv.org/abs/1901.00212.
Byla učená nad třemi různými datovými sadami. Tvým prvním úkolem je si tato data projít a rozmyslet si, pro které obrázky je vhodné takto naučené sítě použít.

###  <span style="color:green">ÚKOL 11: DATOVÉ SADY</span> 

1. Prohlédni si obrázky, na kterých byla učena neurální síť, již budeme používat. Jména modelů odpovídají jménu datové sady.

 * **places2** -- různá prostředí: http://places2.csail.mit.edu/explore.html
 * **celeba** -- tváře známých osobností: http://mmlab.ie.cuhk.edu.hk/projects/CelebA.html
 * **psv** -- paris street view: http://www.cs.ucf.edu/~aroshan/index_files/Dataset_PitOrlManh/images/






###  <span style="color:green">ÚKOL 12: ODSTRANĚNÍ TEXTU 2</span> 

1. Podle úkolu 10 v nové buňce odstraň text z obrázku *cat_text.png*. V tomto případě ale použij neurální síť. Oba výsledky porovnej. Na výběr máš z modelů `places2`, `celeba` a `psv`.

        inpainted = cnn_inpaint(img, mask, model_name='MODEL')
        
2. Novou dokreslovací metodu můžeš zkusit i na dalších obrázcích a maskách z předchoích úkolů.

###  <span style="color:green">ÚKOL 13: TANCUJÍCÍ KŮŇ</span> 

1. V nové buňce překresli koně i s jezdcem na obrázku *horse.png*. Masku si vytvoř obdélníkem nebo elipsou.
1. Porovnej výsledek difúze a neurální sítě. Vyzkoušej také různé modely neurální sítě.

## 3.2 Automatická detekce objektů

Do této chvíle jsme detekovali objekty ručně nebo na základě jejich barvy. Teď tento proces zkusíme zautomatizovat.
V součastnosti se pro hledání komplexních objektů v obraze využívají hlavně statistické metody. Tady je příklad, co si pod tím představit:

*Základem k použití jakékoliv statistické metody je mít velké množství dat. Řekněme, že zrovna my máme stovky tisíc obrázků koček. Z toho datové sady vytvoříme něco jako **průměrnou kočku**. Průměrná kočka není obrázkem, ale velkým množstvím čísel, které popisují vlastnosti obrázku. Těmto hodnotám říkáme obrazové **deskriptory**. Pro každý nový obrázek spočítáme deskriptory a porovnáme je s průměrnou kočkou. Tak zjistíme, zda na obrázku je nebo není nějaká kočka.*

Přesně to si teď vyzkoušíme. Nebudeme v obrazu hledat kočky, ale osoby. K popisu obrázků použijeme HOG desktiptory. **Průměrnou postavu** získáme již z připraveného modelu z knihovny OpenCV. Pro její nahrání spusť následující buňku.




```python
""" DEFINICE """

# nahrání HOG deskriptorů pro osoby
hog = cv2.HOGDescriptor()
hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())
```

###  <span style="color:green">ÚKOL 14: LIDÉ NA FOTKÁCH</span> 

1. V nové buňce vypiš obsah složky *people*
1. Spusť následující buňku a pozoruj, jak se daří detekovat osoby na fotkách v této složce.
1. Použij dokreslovací algoritmus na smazání osob z fotek






```python
%matplotlib inline
path = 'people'

for image_path in os.listdir(path):
    img = io.imread(os.path.join(path, image_path))
    #img = transform.resize(img, np.min(400, img.shape[1]))

    # detekce osob
    (rects, weights) = hog.detectMultiScale(img, winStride=(4, 4),
        padding=(8, 8), scale=1.05)

    # definice masky
    mask = np.zeros(img.shape[:2])
    image = img.copy()
    for (x, y, w, h) in rects:
        image = cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 2)
        mask = mask + pfi.rectangular_mask(img, (x, y), (x+w, y+h), show=False)
    mask = (mask > 0).astype(np.uint8)
    mask = np.expand_dims(mask, -1)
    
    """TODO 3 """
    'dokresli a zobraz obrázek' 
    
    plt.figure(figsize = (20, 20))
    plt.imshow(image)
    plt.show()
    
    plt.figure(figsize = (20, 20))
    plt.imshow(mask)
    plt.show()
    
    res = pfi.inpaint_hifill(img, mask)

    
    plt.figure(figsize = (20, 20))
    plt.imshow(res)
    plt.show()
    
    break
```


```python
img.shape
```

**<span style="color:blue">UŽ UMÍME:</span>**  
 > **<span style="color:blue">10. víme, co ovlivňuje chování neurální sítě</span>**  
 > **<span style="color:blue">11. použít neurální síť EdgeConnect k dokreslení obrázků</span>**  
 > **<span style="color:blue">12. automaticky detekovat osoby v obraze</span>**  

## 4. Hřiště podruhé

Teď už umíš všechno, co je potřeba pro dokreslování obrázků. Inspiruj se kódem v předchozích úkolech a samostatně dokresli i další obrázky.

###  <span style="color:green">ÚKOL 15: EIFFELOVA VĚŽ</span> 

1. Zahraj si na Davida Copperfielda a nech zmizet Eiffelovu věž z obrázku *images/eiffel_tower.jpg*.

###  <span style="color:green">ÚKOL 16: SAGRADA FAMILIA</span> 

1. Jak by vypadala Barcelona bez chrámu Sagrada Familia? Zkus to na obrázku *sagrada_familia.jpg*.

###  <span style="color:green">ÚKOL 17: BIKER NA TRÁVNÍKU</span> 

1. Vyžeň cyklistu z trávníku na obrázku *bike.jpg*.

###  <span style="color:green">ÚKOL 18: HOLUB NA DRÁTU</span> 

1. V nové buňce smaž z obrázku *pigeon.jpg* holuba sedícího na drátech. Použij k tomu libovolnou metodu dokreslení i detekce masky.

###  <span style="color:green">ÚKOL 19: DÁMA S DEŠTNÍKEM</span> 

1. V nové buňce uprav obrázek *umbrella.png*. Pomocí masky vyřízni červený deštník z obrázku a dokresli nebe za ním.
1. Překresli také ženu držící deštník. Více masek je možné sloučit pomocí operátoru '|'

        mask = mask1 | mask2
    


Tento skript vznikl pro prezentaci oboru Vizuální informatika na Fakultě informatiky MU.  
Dotazy nebo připomínky pište na email autora xlux@fi.muni.cz, nebo na email celé skupiny cbia@fi.muni.cz.

https://cbia.fi.muni.cz/  
https://www.fi.muni.cz/





```python

```
