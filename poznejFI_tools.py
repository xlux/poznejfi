import numpy as np
from matplotlib import pyplot as plt
from matplotlib import cm

from skimage import morphology, color, io
import cv2

import os

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
os.environ["KMP_WARNINGS"] = "FALSE" 

def imshow(img, mask=None):
    """
    Customized visualization of 2D image data
    
    In particular, the pixel coordinates are displayed as nonnegative integers 
    and the plot axes are completely disabled.
    
    :param img:     input image
    :param imtype:  one of 'gray', 'labels', 'mask'
    :param bcg:     background of the label image or the binary mask 

    """
    _, ax = plt.subplots(figsize=(12,9))
    if mask is not None:
        # colored labels
        cmap = cm.get_cmap('Set1').copy()
        cmap.set_bad(color='black')

        color_mask = color.label2rgb(mask,
                               colors=cmap.colors,
                               image=img,
                               bg_label=0,
                               alpha=.6)
        plt.imshow(color_mask, interpolation='none')
    
    else:
        # casual image
        plt.imshow(img, 'gray', interpolation='none')
        
    #ax.format_coord = lambda x, y: 'x={:.0f}, y={:.0f}'.format(abs(x), abs(y))
    plt.axis('on')
    
'''        
def imshow(img, imtype='gray', bcg=None):
    """
    Customized visualization of 2D image data
    
    In particular, the pixel coordinates are displayed as nonnegative integers 
    and the plot axes are completely disabled.
    
    :param img:     input image
    :param imtype:  one of 'gray', 'labels', 'mask'
    :param bcg:     background of the label image or the binary mask 

    """
    _, ax = plt.subplots(figsize=(12,9))
    if imtype == 'labels':
        # colored labels
        cmap = cm.get_cmap('tab20b')
        cmap.set_bad(color='black')

        color_mask = color.label2rgb(img,
                               colors=cmap.colors,
                               image=bcg,
                               bg_label=0,
                               alpha=.8)
        plt.imshow(color_mask, interpolation='none')
        
    elif imtype == 'mask':
        # binary mask ploted in a red colour
        mask = img > 0
        color_mask = color.label2rgb(mask, image=bcg, alpha=.3)
        plt.imshow(color_mask, interpolation='none')
    
    elif imtype == 'gray':
        # casual grayscale image
        plt.imshow(img, 'gray', interpolation='none')
        
    #ax.format_coord = lambda x, y: 'x={:.0f}, y={:.0f}'.format(abs(x), abs(y))
    plt.axis('on')
''' 

def show_pixels(img):
    """
    funkce vytvoří rastr, ve kterém jsou dobře vidět
    jednotlivé pixely a jejich indexování
    """
    
    ax = plt.gca()

    # Nastavení popisků
    ax.set_ylabel('souřadnice X', fontweight='bold')
    ax.set_xlabel('souřadnice Y', fontweight='bold')
    ax.set_xticks(np.arange(0, img.shape[1], 1))
    ax.set_yticks(np.arange(0, img.shape[0], 1))

    
    # Vytoření mřížky
    ax.set_xticks(np.arange(-.5, img.shape[1], 1), minor=True)
    ax.set_yticks(np.arange(-.5, img.shape[0], 1), minor=True)
    ax.grid(which='minor', color='gray')
    plt.imshow(img, 'gray')
    plt.show()
    
    
def get_rectangular_mask(img, A, B):
    
    # read coordinates
    xA, yA = A
    xB, yB = B

    # create mask
    mask = np.zeros(img.shape[:2], np.uint8)
    mask[yA:yB, xA:xB] = 255

    return mask

def get_rectangular_mask(img, A, B):
    
    # read coordinates
    xA, yA = A
    xB, yB = B

    # create mask
    mask = np.zeros(img.shape[:2], np.uint8)
    mask[yA:yB, xA:xB] = 255

    return mask
    
    
def rectangular_mask(img, A, B, show=True):
    img_copy = img.copy()
    
    xA, yA = A
    xB, yB = B

    # rozměry obrázku
    x_max, y_max, depth = img.shape

    cv2.rectangle(img_copy, (xA, yA), (xB, yB), (0, 255, 0), 1)

    mask = np.zeros((x_max, y_max), np.uint8)
    mask[yA:yB, xA:xB] = 255

    if show:
        plt.figure(figsize = (20, 20))
        plt.imshow(img_copy)
        plt.show()

    return mask


def elliptical_mask(img, A, B, show=True):
    img_copy = img.copy()
    
    xA, yA = A
    xB, yB = B
    center_coordinates = int(np.floor(np.mean([xA, xB]))), int(np.floor(np.mean([yA, yB])))

    axes_length = np.floor(np.abs([xA - xB])) // 2, np.floor(np.abs([yA - yB])) // 2

    # rozměry obrázku
    x_max, y_max, depth = img.shape

    cv2.ellipse(img_copy,
                center_coordinates,
                axes_length, 
                0, 0, 360, (0, 255, 0), 4) 

    mask = np.zeros((x_max, y_max), np.uint8)
    cv2.ellipse(mask,
                center_coordinates,
                axes_length, 
                0, 0, 360, 255, -1) 

    if show:
        plt.figure(figsize = (20, 20))
        plt.imshow(img_copy)
        plt.show()

    return mask

def dilate(mask, size=5):
    return morphology.dilation(mask, morphology.disk(size))

'''
def get_color_mask(img, col, dilate=5, color_range=60):
    """
    funkce vytvoří masku obrazu 'img'
    maska obsahuje všechny pixely, jejichž barva není od barvy 'col'
    vzdálená více jak 'color_range' odstínů ve všech barevných kanálech
    """
    
    if len(img.shape) == 2:
        img = color.gray2rgb(img)
        
    assert len(img.shape) == 3
    
    # definice rozsahu barev
    color_lim = [[col[0]-color_range, col[0]+color_range],
                 [col[1]-color_range, col[1]+color_range],
                 [col[2]-color_range, col[2]+color_range]]
    
    # rozložení obrázku na tři barevné kanály
    red, green, blue = np.split(img[:, :, :3], 3, axis=-1)

    # vytvoření masky
    mask = (color_lim[0][0] < red) & (red < color_lim[0][1]) & \
           (color_lim[1][0] < green) & (green < color_lim[1][1]) & \
           (color_lim[2][0] < blue) & (blue < color_lim[2][1])
    
    mask = np.squeeze(mask)
    #mask = morphology.opening(mask, morphology.disk(dilate))
    mask = morphology.dilation(mask, morphology.disk(dilate))
    
    return mask
'''

def get_color_mask(img, col, tolerance=30):
    """
    funkce vytvoří masku obrazu 'img'
    maska obsahuje všechny pixely, jejichž barva není od barvy 'col'
    vzdálená více jak 'color_range' odstínů ve všech barevných kanálech
    """
    
    if len(img.shape) == 2:
        img = color.gray2rgb(img)
        
    assert len(img.shape) == 3
    
    col = np.array(col).reshape((1, 1, 3))
    mask = np.mean(np.abs(img - col), axis=-1)
    
    return mask < tolerance

import cv2
import numpy as np
import tensorflow as tf
import glob 
import argparse
import os


INPUT_SIZE = 512  # input image size for Generator
ATTENTION_SIZE = 32 # size of contextual attention


def sort(str_lst):
    return [s for s in sorted(str_lst)]

# reconstruct residual from patches
def reconstruct_residual_from_patches(residual, multiple):
    residual = np.reshape(residual, [ATTENTION_SIZE, ATTENTION_SIZE, multiple, multiple, 3])
    residual = np.transpose(residual, [0,2,1,3,4])
    return np.reshape(residual, [ATTENTION_SIZE * multiple, ATTENTION_SIZE * multiple, 3])

# extract image patches
def extract_image_patches(img, multiple):
    h, w, c = img.shape
    img = np.reshape(img, [h//multiple, multiple, w//multiple, multiple, c])
    img = np.transpose(img, [0,2,1,3,4])
    return img

# residual aggregation module
def residual_aggregate(residual, attention, multiple):
    residual = extract_image_patches(residual, multiple * INPUT_SIZE//ATTENTION_SIZE)
    residual = np.reshape(residual, [1, residual.shape[0] * residual.shape[1], -1])
    residual = np.matmul(attention, residual)
    residual = reconstruct_residual_from_patches(residual, multiple * INPUT_SIZE//ATTENTION_SIZE)
    return residual

# resize image by averaging neighbors
def resize_ave(img, multiple):
    img = img.astype(np.float32)
    img_patches = extract_image_patches(img, multiple)
    img = np.mean(img_patches, axis=(2,3))
    return img

# pre-processing module
def pre_process(raw_img, raw_mask, multiple):

    raw_mask = raw_mask.astype(np.float32) / 255.
    raw_img = raw_img.astype(np.float32)

    # resize raw image & mask to desinated size
    large_img = cv2.resize(raw_img,  (multiple * INPUT_SIZE, multiple * INPUT_SIZE), interpolation = cv2. INTER_LINEAR)
    large_mask = cv2.resize(raw_mask, (multiple * INPUT_SIZE, multiple * INPUT_SIZE), interpolation = cv2.INTER_NEAREST)

    # down-sample large image & mask to 512x512
    small_img = resize_ave(large_img, multiple)
    small_mask = cv2.resize(raw_mask, (INPUT_SIZE, INPUT_SIZE), interpolation = cv2.INTER_NEAREST)

    # set hole region to 1. and backgroun to 0.
    small_mask = 1. - small_mask
    return large_img, large_mask, small_img, small_mask


# post-processing module
def post_process(raw_img, large_img, large_mask, res_512, img_512, mask_512, attention, multiple):

    # compute the raw residual map
    h, w, c = raw_img.shape
    low_base = cv2.resize(res_512.astype(np.float32), (INPUT_SIZE * multiple, INPUT_SIZE * multiple), interpolation = cv2.INTER_LINEAR)
    low_large = cv2.resize(img_512.astype(np.float32), (INPUT_SIZE * multiple, INPUT_SIZE * multiple), interpolation = cv2.INTER_LINEAR)
    residual = (large_img - low_large) * large_mask

    # reconstruct residual map using residual aggregation module
    residual = residual_aggregate(residual, attention, multiple)

    # compute large inpainted result
    res_large = low_base + residual
    res_large = np.clip(res_large, 0., 255.)

    # resize large inpainted result to raw size
    res_raw = cv2.resize(res_large, (w, h), interpolation = cv2.INTER_LINEAR)

    # paste the hole region to the original raw image
    mask = cv2.resize(mask_512.astype(np.float32), (w, h), interpolation = cv2.INTER_LINEAR)
    mask = np.expand_dims(mask, axis=2)
    res_raw = res_raw * mask + raw_img * (1. - mask)

    return res_raw.astype(np.uint8)


def inpaint(raw_img, 
            raw_mask, 
            sess, 
            inpainted_512_node, 
            attention_node, 
            mask_512_node, 
            img_512_ph, 
            mask_512_ph, 
            multiple):

    # pre-processing
    img_large, mask_large, img_512, mask_512 = pre_process(raw_img, raw_mask, multiple)

    # neural network
    inpainted_512, attention, mask_512  = sess.run([inpainted_512_node, attention_node, mask_512_node], feed_dict={img_512_ph: [img_512] , mask_512_ph:[mask_512[:,:,0:1]]})

    # post-processing
    res_raw_size = post_process(raw_img, img_large, mask_large, \
                 inpainted_512[0], img_512, mask_512[0], attention[0], multiple)

    return res_raw_size

def inpaint_diffusion(img, mask):
    return cv2.inpaint(img, mask.astype(np.uint8), 5, cv2.INPAINT_NS)


def inpaint_AI(raw_img, raw_mask):
    
    if raw_img.shape[:2] == raw_mask.shape[:2]:
        
        if len(raw_mask.shape) == 2:
            raw_mask = np.expand_dims(raw_mask, axis=-1)
        
        if (raw_img.shape[2] == 3) and (raw_mask.shape[2] == 1):
            raw_mask = np.concatenate([raw_mask]*3, axis=-1)
            
    assert raw_img.shape == raw_mask.shape
    
    # inverse logic
    raw_mask = 255 - raw_mask
        
    MULTIPLE = 6
    with tf.Graph().as_default():
        with open('pb/hifill.pb', "rb") as f:
            output_graph_def = tf.compat.v1.GraphDef()
            output_graph_def.ParseFromString(f.read())
            tf.import_graph_def(output_graph_def, name="")

        with tf.compat.v1.Session() as sess:
            init = tf.compat.v1.global_variables_initializer()
            sess.run(init)
            image_ph = sess.graph.get_tensor_by_name('img:0')
            mask_ph = sess.graph.get_tensor_by_name('mask:0')
            inpainted_512_node = sess.graph.get_tensor_by_name('inpainted:0')
            attention_node = sess.graph.get_tensor_by_name('attention:0')
            mask_512_node = sess.graph.get_tensor_by_name('mask_processed:0')

            #raw_img = cv2.imread(path_img)
            #raw_mask = cv2.imread(path_mask)
            inpainted = inpaint(raw_img,
                                raw_mask,
                                sess,
                                inpainted_512_node,
                                attention_node,
                                mask_512_node,
                                image_ph,
                                mask_ph,
                                MULTIPLE)
            
    return inpainted


from tkinter import *
from PIL import Image, ImageTk, ImageGrab

def increase_brightness(img, level=.1):
    inv_img = (255 - img) * (1 - level)
    return 255 - inv_img.astype(np.uint8)


class DrawingWindow:
    BRUSH_SIZE = 40
    BRUSH_COLOR = 'black'
    BTN_PADDING = 2
    OFFSET = 1

    def __init__(self, img, filename='tmp_DrawingWindow.png'):
        
        # output filename
        self.filename = filename
        
        # init window
        self.root = Tk()
        self.root.wm_title('Draw mask')
        
        # load image
        #self.img_name = img_name
        #img = Image.open(img_name)
        #img = io.imread(img_name)
        img = increase_brightness(img)
        img = Image.fromarray(img)
        
        self.img_render = ImageTk.PhotoImage(img)

        # init canvas
        self.canvas = Canvas(self.root, width=img.width, height=img.height)
        self.canvas.create_image(0, 0, anchor=NW, image=self.img_render)
        self.canvas.bind("<B1-Motion>", self.paint)
        self.canvas.pack(expand=YES, fill=BOTH)

        # add buttons
        f1 = Frame(self.root)
        btn_clear = Button(f1, text="Clear", command=self.clear)
        btn_clear.pack(expand=True, side=LEFT,
                       padx=DrawingWindow.BTN_PADDING, pady=DrawingWindow.BTN_PADDING)
        btn_save = Button(f1, text="Get Mask", command=self.save)
        btn_save.pack(expand=True, side=LEFT,
                      padx=DrawingWindow.BTN_PADDING, pady=DrawingWindow.BTN_PADDING)

        f1.pack()

        # mainloop
        self.root.mainloop()

    def paint(self, event):
        x1, y1 = (event.x - DrawingWindow.OFFSET), (event.y - DrawingWindow.OFFSET)
        x2, y2 = (event.x + DrawingWindow.OFFSET), (event.y + DrawingWindow.OFFSET)
        self.canvas.create_line(x1, y1, x2, y2, 
                                width=DrawingWindow.BRUSH_SIZE,
                                fill=DrawingWindow.BRUSH_COLOR, 
                                capstyle=ROUND, smooth=True)

    def save(self):
        '''
        filename, extension = self.img_name.split('.')
        filename = filename + "_mask." + extension
        '''
        
        filename= self.filename

        x1, y1 = self.canvas.winfo_rootx() + self.canvas.winfo_x() + 1, self.canvas.winfo_rooty() + self.canvas.winfo_y() + 1
        x2, y2 = x1 + self.canvas.winfo_width() - 2, y1 + self.canvas.winfo_height() - 2

        ImageGrab.grab().crop((x1, y1, x2, y2)).save(filename)
        
        # close
        self.root.destroy()
        
        

    def clear(self):
        self.canvas.delete("all")
        self.canvas.create_image(0, 0, anchor=NW, image=self.img_render)

        
        


def draw_mask(img):
    filepath = 'tmp.png'
    
    
    DrawingWindow(img, filepath)
    
    BLACK = np.zeros([1, 1, 3])
    
    if os.path.isfile(filepath):
        mask = io.imread(filepath)
        mask = np.equal(mask, BLACK)[:, :, 0] * 255
        
        os.remove(filepath)

    
    return mask





    