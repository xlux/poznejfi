# source markdowns
MDS = $(wildcard inpainting_*.md) 
# source ipython notebooks, cleaned, created manually
SOURCE_IPYNBS = $(wildcard inpainting_*_source.ipynb) 
NEW_MDS = $(patsubst %_source.ipynb, %.md, $(SOURCE_IPYNBS))

# ipython notebooks for students
STUDENT_IPYNBS = $(patsubst %.md, %.ipynb, $(MDS))
# ipython notebooks with solution
SOLUTION_IPYNBS = $(patsubst %.md, %_solution.ipynb, $(MDS))

.PHONY: all, clean, md

all:

	$(MAKE) student_notebooks
	$(MAKE) solution_notebooks

md:
	$(MAKE) source_mds

student_notebooks: $(STUDENT_IPYNBS)
solution_notebooks: $(SOLUTION_IPYNBS)
source_mds: $(NEW_MDS)


%.ipynb: %.md # student notebook
	@echo STUDENT NTB Convert to ipynb and remove cells with solutions and exercise scaffoldings. $<
	jupytext --to notebook --output $(basename $<).ipynb $<
	jupyter nbconvert \
	  --to notebook \
	  --clear-output \
	  --RegexRemovePreprocessor.enabled=True \
	  --RegexRemovePreprocessor.patterns=".*SOLUTION.*"\
	  --RegexRemovePreprocessor.patterns=".*SCAFFOLDING.*"\
	  --ClearMetadataPreprocessor.enabled=True \
	  $@ \
	  --output $(notdir $@)
	

%_solution.ipynb: %.md # solution notebook
	@echo SOLUTION NTB Convert to ipynb and remove cells with notes. $<
	jupytext --to notebook --output $(basename $<)_solution.ipynb $<
	jupyter nbconvert \
	  --to notebook \
	  --clear-output \
	  --RegexRemovePreprocessor.enabled=True \
	  --RegexRemovePreprocessor.patterns=".*SCAFFOLDING.*"\
	  --ClearMetadataPreprocessor.enabled=True \
	  $@ \
	  --output $(notdir $@)


%.md: %_source.ipynb # source markdown
	@echo SOURCE MD Convert source notebook to markdown, keep all cells. $<
	jupyter nbconvert --clear-output $< 
	jupyter nbconvert \
	  --to markdown \
	  $< \
	  --output $(notdir $@)



clean:
	$(RM) $(STUDENT_IPYNBS) $(SOLUTION_IPYNBS)

requirements:
