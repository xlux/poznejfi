# **Poznej FI 2022** - Image InPainting 2


Vyfotil jsi skvělou fotku, ale do záběru se ti postavil horlivý turista? Chceš opravit obrázek, který je překrytý textem? Zajímá tě, jak automaticky detekovat objekty v obraze a jak je z tohoto obrazu vymazat? V tomto skriptu si takové věci vyzkoušíme.
Odborně se technice dokreslování říká **Image Inpainting**. Víc se o ní můžeš dočíst například na stránkách Wikipedie: https://en.wikipedia.org/wiki/Inpainting.

*"Inpainting je proces rekonstruování ztracených nebo poškozených částí obrazu nebo videa. V případě uměleckých děl je tento úkol realizován zkušenými malíři a reustauratéry. V digitálním světě, Inpainting odkazuje na aplikaci nebo specializovaný algoritmus, který automaticky doplní chybějící nebo poškozenou část obrazu."*


### **Obsah tutorialu**<br>

> #### 1. Základy práce s obrazem
> #### **2. Hledání objektů v obraze**
> #### 3. Pokročilé dokreslovací metody

<br>     

## 2. Hledání objektů v obraze

Dokreslení obrazu se skládá ze dvou kroků:
* **1.Výběr oblasti**, kterou chceme dokreslit. 
* **2. Spuštění dokreslovací metody**, která překryje vybranou oblast novými hodnotami. 

V tomto notebooku se budeme zabývat hlavně *prvním krokem*. Ukážeme si, jak hledat oblasti k dokreslení pomocí **souřadnic** a **barev**.


```python
# import knihoven
import os
import numpy as np
from skimage import io

%matplotlib tk
import poznejFI_tools as fi
```

Oblast v obraze je možné reprezentovat pomocí **binární masky**. Ta nám udává, které pixely patří do oblasti a které do ní nepatří. V masce mají pixely hodnoty **True** nebo **False**. Masku v promněnné `mask` si můžeme zobrazit jako běžný obrázek pomocí volání:

        fi.imshow(mask)



nebo ji můžeš zobrazit jako červeně zvýrazněnou oblast v původním obrázku `img`.

        fi.imshow(img, mask)


```python
img_name = 'images/snowboard.png'

# načtení obrázku
img = io.imread(img_name)

# vytvoření masky
A = [75, 20]
B = [125, 40]
mask = fi.get_rectangular_mask(img, A, B)

# zobrazení masky
fi.imshow(mask)
fi.imshow(img, mask)
```


###  <span style="color:green">ÚKOL 6: VÝBĚR SNOWBOARDISTY</span> 

1. Uprav souřadnice bodů **A** a **B** a pomocí obdélníkové masky vyber snowboardistu na obrázku *snowboard.png*. Masku si zobraz.

        A = [Ax, Ay]
        B = [Bx, By]
        mask = get_rectangular_mask(img, A, B)          


```python
""" ÚKOL 6.1 - SOLUTION """

A = [30, 25]
B = [65, 60]
mask = fi.get_rectangular_mask(img, A, B)

fi.imshow(img, mask)
```

### Výběr masky pomocí barvy

V některých případech se nám může hodit vybrat objekty podle jejich barvy.  Funkce `fi.get_color_mask(img, color)` vytvoří masku obrazu, která obsahuje všechny pixely s odstínem blízkým zadané barvě `color`. Hodnota `tolerance` udává, o kolik odstínů se může barva pixelu lišit od barvy `color`, aby byl ještě vybrán.


```python
img_name = 'images/lentilky.png'

# načtení a zobrazení obrázku
img = io.imread(img_name)
fi.imshow(img)
```

###  <span style="color:green">ÚKOL 7: MASKA PODLE BAREV</span> 

1. Zadej do následující buňky hodnoty barev, které jsi zjistil v úkolu **1.5: VŠECHNY BARVY LENTILEK**. Podle nich získáme masky lentilek.

1. Pokud by masky pro nějaké barvy nebyly přesné, uprav definici barvy.

1. Uprav hodnotu proměnné `tolerance`, o kterou se každá maska zvětšuje. Pozoruj změny.
        


```python
blue = [0, 0, 0]
orange = [0, 0, 0]
red = [0, 0, 0]
green = [0, 0, 0]
yellow = [0, 0, 0]
```


```python
''' SOLUTION 7.1 '''

blue = [0, 180, 230]
orange = [250, 120, 0]
red = [250, 60, 50]
green = [140, 230, 30]
yellow = [250, 230, 0]
```


```python
'vyber barvu lentilek, pro kterou chceš zobrazit masku'
'nastav barevnou toleranci'
color = blue
tolerance = 100

# získání masky
mask = fi.get_color_mask(img, color, tolerance)

# zobrazení masky
fi.imshow(img, mask)
```

###  <span style="color:green">ÚKOL 8: MALOVÁNÍ JEDNOU BARVOU</span> 

1. nadefinuj si barvu `NEW_COLOR`, kterou lentilky přebarvíš. Jako první je zkus přemalovat barvou pozadí.

1. Ne všechny masky se dají přesně vybrat barvou. Pro jejich vylepšení je můžeme nafouknout takzvanou **dilatací**. Vyzkoušej různé hodnoty parametru `size` pro nafouknutí masek.

        mask_large = fi.dilate(mask, size)
        




```python
# definování nové barvy
NEW_COLOR = None

# velikost nafouknutí (max 40)
size = 0
```


```python
''' SOLUTION 8.1 '''

# definování nové barvy
NEW_COLOR = [255, 255, 255]

# velikost nafouknutí (max 40)
size = 10
```


```python
# zvětšení masky
mask_large = fi.dilate(mask, size)

# přemalování lentilek v masce na novou barvu
inpainted = img.copy();
inpainted[mask_large] = NEW_COLOR

# zobrazení výsledku
fi.imshow(inpainted)
```

###  <span style="color:green">ÚKOL 8: PŘEKRESLENÍ LENTILEK DIFFUZÍ</span> 
1. Do nové buňky zkopíruj předchozí kód a nahraď v něm místo, kde se lentilky překreslují bílou barvou, příkazem:

        inpainted = fi.inpaint_diffusion(img, mask)
        
2. Podle výsledku si rozmysli, jak funguje difúzní dokreslovací metoda. Svůj názor na její fungování prodiskutuj s ostatními.


```python
''' SOLUTION '''

# zvětšení masky
mask_large = fi.dilate(mask, size)

# přemalování lentilek v masce na novou barvu
inpainted = fi.inpaint_diffusion(img, mask_large)

# zobrazení výsledku
fi.imshow(inpainted)
```

**Jak že je to s tou difúzí?**  
Po aplikaci difúzní metody (možná) lentilky dané barvy zmizely, ostatní ale vypadají jako po dešti. Jsou rozpité.
Metoda nám rozmazala barvy a stíny z okrajů dokreslované oblasti směrem do středu.
Proto tento typ dokreslovací metody nazýváme **difúzní** a její výsledek je velmi podobný procesu rozpouštění.
V tomto případě výsledek nevypadá příliš věrohodně. Metoda sama o sobě ale není špatná. Proti ostatním metodám je například velmi rychlá. Zkusíme ji použít na jiných obrázcích.

###  <span style="color:green">ÚKOL 9: PTÁCI NA OBLOZE</span> 

1. Další úkolem je vyčistit oblohu na obrázku *birds.jpg* od siluet ptáků. Použijeme na to výběr masky pomocí barvy (siluety jsou proti obloze velmi tmavé) a dokreslení difúzí. 

            mask = fi.get_color_mask(img, color, tolerance)
            inpainted = fi.inpaint_diffusion(img, mask)
        
1. Abys z obrazu smazal opravdu všechny tmavé pixely, můžeš masku zvětšit nafouknutím pomocí dilatace.

            large_mask = fi.dilate(mask, size)



```python
img_name = 'images/birds.jpg'

# načtení obrázku
img = io.imread(img_name)

fi.imshow(img)
```


```python
""" ÚKOL 9.1  """
'uprav hodnoty "color" a "dilate" tak, aby dokreslovací metoda '
'fungovala co nejlépe. '

color = [0, 0, 0]
tolerance = 0
size = 0

```


```python
""" ÚKOL 9.1 - SOLUTION """

color = [0, 0, 0]
tolerance = 20
size = 5

```


```python
# získání masky
mask = fi.get_color_mask(img, color, tolerance)

# přemalování objektů difúzní funkcí
mask = fi.dilate(mask, size)
inpainted = fi.inpaint_diffusion(img, mask)

# zobrazení výsledku
fi.imshow(img, mask)
fi.imshow(inpainted)
```

###  <span style="color:green">ÚKOL 10: ODSTRANĚNÍ TEXTU</span> 

1. Dokresli obrázek *cat_text.png*, aby nebyl poškozený textem. Pro správné fungování bude potřeba upravit i hodnotu `tolerance`, která určuje, jak se nejvýše mohou lišit barvy v masce od zadané barvy `color`.



```python
img_name = 'images/cat_text.png'

# načtení obrázku
img = io.imread(img_name)

fi.imshow(img)
```


```python
''' SOLUTION '''

color = [255, 255, 255]
tolerance = 1
size = 1


# získání masky
mask = get_color_mask(img, color, tolerance)

# přemalování objektů difúzní funkcí
mask = fi.dilate(mask, size)
inpainted = fi.inpaint_diffusion(img, mask)

# zobrazení výsledku
fi.imshow(img, mask)
fi.imshow(inpainted)
```

**<span style="color:blue">UŽ DÁLE UMÍME:</span>**  
 > **<span style="color:blue">6. vytvořit obdelníkovou a eliptickou masku</span>**  
  > **<span style="color:blue">7. definovat masku podle barvy</span>**  
 > **<span style="color:blue">8. přebarvit oblast jinou barvou</span>**  
 > **<span style="color:blue">9. použít Inpainting pomocí difuzních metod</span>**  

Tento skript vznikl pro prezentaci oboru Vizuální informatika na Fakultě informatiky MU.  
Dotazy nebo připomínky pište na email autora xlux@fi.muni.cz, nebo na email celé skupiny cbia@fi.muni.cz.

https://cbia.fi.muni.cz/  
https://www.fi.muni.cz/


