# ImageInpainting

Výukový skript, zabývající se dokreslováním obrazu.
Zahrnuje úvod k detekci objetů pomocí barvy objektu a pomocí souřadnice objektu.
Představuje difúzní dokreslovací metodu a dokreslování pomocí neurálních sítí.

Pro instalaci:

1. Nainstaluj balíček Anaconda.

        https://docs.conda.io/projects/conda/en/latest/user-guide/install/

2. Vytvoř environment 'poznejfi'

        conda env create -f poznejfi.yaml
        conda activate poznejfi
        
3. Spusť jupyter lab

        jupyter lab


# Developer notes
## Prerequisites
1. *jupyterlab*, *jupytext* and *nbconvert* packages
```bash
$ pip install jupyterlab
$ pip install jupytext --upgrade
$ pip install nbconvert --upgrade

```

## Compile student and solution notebooks

Convert markdown file `inpainting_X.md` to `inpainting_X.ipynb` and `inpainting_X_solution.ipynb` notebooks.

```bash
$ make all
```

## Edit source markdown file 

1. Copy `inpainting_X_solution.ipynb` to `inpainting_X_source.ipynb`. Change only this file.

2. Tag cells by a string keywords to control which cell will be included in the final files. Keywords are find by RegEx. <br>
- `SOLUTION` - the cell is not exported to `inpainting_X.ipynb` student file.
- `SCAFFOLDING` - the cell is not exported at all.

3. Compile markdown file `inpainting_X.md`.

```bash
$ make md
```



